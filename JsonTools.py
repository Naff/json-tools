#-*- coding:utf-8 -*-
# gaoyiping (iam@gaoyiping.com)
# https://gitee.com/Naff
import json
import wx

class MainForm(wx.Frame):
	def __init__(self, *args, **kwargs):
		wx.Frame.__init__(self, None, title="老高Json不求人", size = (1440, 900))
		# 实例化空间对象
		self._eButton = ExpandButton(self)
		self._rButton = ReduceButton(self)
		self._richbox = RichTextBox(self)
		# 窗口属性
		icon = wx.Icon()
		icon.LoadFile("favicon.ico", wx.BITMAP_TYPE_ICO)
		self.SetIcon(icon)
		self.SetMaxSize((1440, 900))
		self.SetMinSize((1440, 900))
		self.Center()
		self.Show(True)

class ExpandButton(wx.Button):
	def __init__(self, parent):
		wx.Button.__init__(self, parent, label = '美化', pos = (10, 10))
		self.parent = parent
		self.Bind(wx.EVT_BUTTON, self._btnClick)

	def _btnClick(self, e):
		text = self.parent._richbox.GetValue()
		if text:
			try:
				o = json.loads(self.parent._richbox.GetValue())
				s = json.dumps(o, indent = 4)
				s = s.replace('    ', '	').replace(', ', ',')
				self.parent._richbox.SetValue(s)
			except:
				pass

class ReduceButton(wx.Button):
	def __init__(self, parent):
		wx.Button.__init__(self, parent, label = '精简', pos = (120, 10))
		self.parent = parent
		self.Bind(wx.EVT_BUTTON, self._btnClick)

	def _btnClick(self, e):
		text = self.parent._richbox.GetValue()
		if text:
			try:
				o = json.loads(self.parent._richbox.GetValue())
				self.parent._richbox.SetValue(json.dumps(o, separators=(',',':')))		
			except:
				pass

class RichTextBox(wx.TextCtrl):
	def __init__(self, parent):
		self.parent = parent
		wx.TextCtrl.__init__(self, parent, style=wx.TE_MULTILINE|wx.TE_RICH|wx.TE_PROCESS_ENTER, pos = (2, 60), size = (1421, 800))

if __name__ == '__main__':
	app = wx.App()
	window = MainForm()
	app.MainLoop()